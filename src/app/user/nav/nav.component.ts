import { Component, OnInit } from '@angular/core';
import { User } from '../../providers/user';
import { Storage } from '../../providers/storage';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  //#region Variables

  user:any;
  navAnimate: boolean = false;

  //#endregion Variables

  //#region Constructor
  constructor(
    private userService: User,
    private storage: Storage,
    private router: Router
  ) { }
  //#endregion Constructor

  //#region Functions

  init(){
    this.userService.getUserInfoFromStorage().then(user => {
      if(user)
        this.user = user;

    });
  }

  logout(){
    this.storage.clearAll().then(res => {
      if(res)
        this.router.navigateByUrl('login');
    });
  }

  //#endregion Functions

  //#region Events
  ngOnInit() {
    this.init();
  }

  hamburgerClick(){
    this.navAnimate = !this.navAnimate;
  }

  doLogout(){
    this.logout();
  }
  //#endregion Events

}
