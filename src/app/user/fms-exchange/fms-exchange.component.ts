import { Component, OnInit } from '@angular/core';
// import * as $ from 'jquery';
import { User } from '../../providers/user';
import { API } from '../../providers/api';
import { Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { SnotifyService, SnotifyPosition } from 'ng-snotify';
import { Router } from '@angular/router';
declare var $: any;
@Component({
  selector: 'app-fms-exchange',
  templateUrl: './fms-exchange.component.html',
  styleUrls: ['./fms-exchange.component.scss']
})
export class FmsExchangeComponent implements OnInit {

  //#region Variables

  USER_INFO: any;
  AUTH_INFO: any;

  isLoading:boolean = false;
  isPurchaseLoading:boolean = false;
  connectionErrorText: string;

  lstMortgages: any[];
  lstAllMortgages: any[];
  lstRegionsFilter: any[];
  selectedMortgage: any;
  selectedSubMortgage: any;
  selectedQuantity: number = 0;
  //#endregion Variables

  //#region Constructor
  constructor(
    private userServices: User,
    private api: API,
    private notification: SnotifyService,
    private router: Router
  ) {
    this.connectionErrorText = 'There was a problem while fetching your account information. Please check your internet connection and try again';
  }
  //#endregion Constructor

  //#region Functions

  init(){
    this.getPreRequisites();
  }

  getPreRequisites(){
    return new Promise(resolve => {
      this.isLoading = true;
      this.userServices.getTokenFromStorage().then(token => {
        if(token){
          this.AUTH_INFO = token;
          this.userServices.getUserInfoFromStorage().then(userInfo => {
            if(userInfo){
              this.USER_INFO = userInfo;
              this.getMortgages();
              resolve(true);
            }
          })
        }
      }).catch( err => {
        this.router.navigateByUrl('login');
      });
    });
  }

  getMortgages(){
    return new Promise(resolve => {
      let _headers = new Headers({ 'Authorization': 'bearer ' + this.AUTH_INFO.access_token });
      let _options = new RequestOptions({ headers: _headers });
      this.api.get('api/OpenMarket/GetOpenMarketByUserID?userid=' + this.USER_INFO.UserInfo.UserID, null, _options).subscribe(result => {
      if(result){
          this.lstMortgages = result.json();
          this.lstAllMortgages = this.lstMortgages;
          this.selectedMortgage = this.lstMortgages[0];
          this.isLoading = false;
          this.fillFilterByRegionData();
          console.log(this.lstMortgages);
          resolve();
        }
      })
    });
  }

  fillFilterByRegionData(){
    this.lstRegionsFilter = [];
    this.lstMortgages.forEach(mortgage => {
      if(this.lstRegionsFilter.indexOf(mortgage.Result.Region) < 0){
        this.lstRegionsFilter.push(mortgage.Result.Region);
      }
    });
  }

  invesetMortgageByUser(){
    return new Promise(resolve => {
      let _headers = new Headers({ 'Authorization': 'bearer ' + this.AUTH_INFO.access_token });
      let _options = new RequestOptions({ headers: _headers });
      let obj = {
        UserID: this.USER_INFO.UserInfo.UserID,
        UserOpenMarketID: this.selectedSubMortgage.UserOpenMarketID,
        Unit: this.selectedQuantity
      };
      let params = new URLSearchParams();
      for(let p in obj){
          params.set(p, obj[p]);
      }
      this.api.post('api/OpenMarket/BuySharesOnOpenMarket', params, _options).subscribe(
        result => {
          if(result){
              resolve(true);
            }
        },
        err => {
          this.notification.error(this.connectionErrorText, {
            timeout: 5000,
            showProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            position: SnotifyPosition.rightTop
          });
          resolve(false);
        }
      )
    });
  }

  hideDialog(){
    let modal = <HTMLInputElement>document.getElementById('fmsOrderModal');
    let backdrop = <HTMLInputElement>document.getElementsByClassName('modal-backdrop')[0];
    let body = <HTMLBodyElement>document.getElementsByTagName('body')[0];
    modal.classList.remove('show');
    modal.attributes.setNamedItem(modal.attributes.getNamedItem('style')).value = 'display: none';
    modal.setAttribute('aria-hidden', 'true');
    backdrop.remove();
    body.classList.remove('modal-open');
    body.attributes.removeNamedItem('style');
  }

  //#endregion Functions

  //#region Events


  public ngOnInit() {
    this.init();
    // Jquery for content scroll
    var screenHeightv = $(window).height() - 300;
    $(".scroll-content").css("height",screenHeightv);
$(window).load(function(){

});

  }
  modalClose(){
   $('#fmsOrderModal').modal("hide");
   $('#orderBookModal').modal("show");
  }
  buyMortgage(){
    this.isPurchaseLoading = true;
    this.invesetMortgageByUser().then(res => {
      if(res){
        this.isPurchaseLoading = false;
        this.hideDialog();
        $('#fmsOrderModal').modal("hide");
        this.router.navigateByUrl('dashboard/investment?invested=true');
      }
    });
  }

  selectMortgage(mortgage){
    this.selectedMortgage = mortgage;
    if(mortgage.Buy.length > 0)
      this.selectedSubMortgage = mortgage.Buy[0];
  }

  selectSubMortgage(mortgage, submortgage){
    this.selectedMortgage = mortgage;
    this.selectedSubMortgage = submortgage;
  }

  checkInputForFloatingNumbers(){
    var result = String(this.selectedQuantity).includes('.');
    if(result){
      this.selectedQuantity = Number(String(this.selectedQuantity).split('.')[0]);
    }
  }
  toggleCheck = [
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false
  ];

  sorting(index, type){
    this.toggleCheck[index] = !this.toggleCheck[index];
    if(this.toggleCheck[index])
      this.sortByDataType(type, 'asc');
    else
      this.sortByDataType(type, 'desc');
    
  //this.sortingData=!this.sortingData;
  }

  selectedRegion = '';
  applyFilter(region){
    this.lstMortgages = [];
    this.lstAllMortgages.forEach(mortgage => {
      if(mortgage.Result.Region === region){
        this.lstMortgages.push(mortgage);
      }
    });
    this.selectedMortgage = this.lstMortgages[0];
    this.selectedRegion = region;
  }

  clearFilter(){
    this.lstMortgages = this.lstAllMortgages;
    this.selectedMortgage = this.lstMortgages[0];
    this.selectedRegion = '';
  }

  sortByDataType(type, order){
    this.lstMortgages = this.lstAllMortgages;
    switch(type){
      case 'Coupon':
        this.lstMortgages = this.lstMortgages.sort((a,b) => {
          if(order === 'asc') 
            return a.Result.Coupon - b.Result.Coupon;
          if(order === 'desc')
            return b.Result.Coupon - a.Result.Coupon;
        });
      break;
      case 'Rating':
        this.lstMortgages = this.lstMortgages.sort((a,b) => {
          if(order === 'asc') 
            return a.Result.CreditRating > b.Result.CreditRating ? -1 : 1;
          if(order === 'desc')
            return b.Result.CreditRating < a.Result.CreditRating ? 1 : -1;
        });
      break;
      case 'Maturity':
        this.lstMortgages = this.lstMortgages.sort((a,b) => {
          if(order === 'asc') 
            return a.Result.MaturityDate > b.Result.MaturityDate ? -1 : 1;
          if(order === 'desc')
            return b.Result.MaturityDate < a.Result.MaturityDate ? 1 : -1;
        });
      break;
    }
    
  }

  //#endregion Events

}
