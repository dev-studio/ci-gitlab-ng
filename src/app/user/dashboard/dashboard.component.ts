import { Component, OnInit } from '@angular/core';
import { RouterModule, Routes, Router } from '@angular/router';
import { Chart } from 'angular-highcharts';
import { User } from '../../providers/user';
import { API } from '../../providers/api';
import { Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { SnotifyService, SnotifyPosition } from 'ng-snotify';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent  {

 isLoading:boolean = false;
 chart:any;
 pieChart:any;
 geoChartData:any;

  //#region Variables

  USER_INFO: any;
  AUTH_INFO: any;

  DashboardData: {
    AmountInvested: number,
    CashBalance: number,
    TotalNumberOfInvestments: number,
    VIVABalance: number,
    TotalYield: number,
    EstimatedAnnualIncome: number
  } = {
    AmountInvested: 0,
    CashBalance: 0,
    TotalNumberOfInvestments: 0,
    VIVABalance: 0,
    TotalYield: 0,
    EstimatedAnnualIncome: 0
  }

  GraphData: any[];

  regionData:any[];
  colors:string[] = [
    '#e11b49',
    '#52b974',
    '#555ac8',
    '#c33980',
    '#32a3ea',
    '#673AB7',
    '#D32F2F',
    '#009688',
    '#303F9F',
    '#5D4037',
    '#607D8B',
    '#AFB42B',
    '#8BC34A',
    '#0097A7',
    '#448AFF',
    '#C2185B'
  ];


  //#endregion Variables

  //#region Constructor
  constructor(
    private userServices: User,
    private api: API,
    private notification: SnotifyService,
    private router: Router
  ) { }
  //#endregion Constructor

  //#region Functions

  init(){
    this.getPreRequisites();
  }

  getPreRequisites(){


    return new Promise(resolve => {
      this.isLoading = true;
        this.userServices.getTokenFromStorage().then(token => {
          if(token){
            this.AUTH_INFO = token;
            this.userServices.getUserInfoFromStorage().then(userInfo => {
              if(userInfo){
                this.USER_INFO = userInfo;
                this.getDashboardData().then(res => {
                  this.getRegionData().then(data => {
                    this.isLoading = false;
                  });
                  this.getGraphData();
                });
                resolve(true);
              } else {
                resolve(false);
              }
            })
          } else {
            resolve(false);
          }
        }).catch( err => {
          this.router.navigateByUrl('login');
        });
    });
  }

  getDashboardData(){
    return new Promise(resolve => {
      let _headers = new Headers({ 'Authorization': 'bearer ' + this.AUTH_INFO.access_token });
      let _options = new RequestOptions({ headers: _headers });
      this.api.get('api/User/GetUserDashboard?userid=' + this.USER_INFO.UserInfo.UserID, null, _options).subscribe(result => {
      if(result){
          var data = result.json();
          this.DashboardData.AmountInvested =data.User.AmountInvested;
          this.DashboardData.CashBalance =data.User.CashBalance;
          this.DashboardData.TotalNumberOfInvestments =data.User.TotalNumberOfInvestments;
          this.DashboardData.VIVABalance =data.User.VIVABalance;
          this.DashboardData.TotalYield =data.User.TotalYield;
          this.DashboardData.EstimatedAnnualIncome =data.User.EstimatedAnnualIncome;
          this.GraphData = data.DailyYield;
          resolve();
        }
      })
    });
  }

  getRegionData(){
    return new Promise(resolve => {
      let _headers = new Headers({ 'Authorization': 'bearer ' + this.AUTH_INFO.access_token });
      let _options = new RequestOptions({ headers: _headers });
      this.api.get('api/Investment/GetRegionsOfInvestment?userid=' + this.USER_INFO.UserInfo.UserID, null, _options).subscribe(result => {
      if(result){
          var data = result.json();
          this.regionData = [];
          data.forEach(region => {
            this.regionData.push({
              Continent: region.Continent.split('-')[1],
              ContinentID: region.Continent.split('-')[0],
              ID: region.ID,
              Percent: region.Percent
            });
          });
          var pieChart = [];
          var counter = 0;
          this.regionData.forEach(region => {
            pieChart.push({
              name: region.Continent,
              y: region.Percent,
              sliced: false,
              selected: false,
              color: this.colors[counter]
            });
            counter++;
          });

         this.pieChart = new Chart({
            chart: {
              backgroundColor:'#313343',
              plotBackgroundColor: null,
              plotBorderWidth: null,
              plotShadow: false,
              type: 'pie',
              borderRadius: 7,
          },
          title: {
              text: ''
          },
          tooltip: {
              pointFormat: '<b>{point.percentage:.1f}%</b>'
          },
          plotOptions: {
              pie: {
                  allowPointSelect: true,
                  cursor: 'pointer',
                  dataLabels: {
                      enabled: false,

                  }
              }
          },
          yAxis: {
            title: {
              text: 'Total percent market share'
            }
          },


          series: [{
              name: '',
              size: '100%',
              innerSize: '60%',
              data: pieChart
          },
         ]
          });

          var geoChartDataArray = [];
          geoChartDataArray.push(
            ['Region', 'Invested']
          );
          this.regionData.forEach(region => {
            if(region.ContinentID < 100){
              geoChartDataArray.push(
                [ "0" + String(region.ContinentID), region.Percent]
              );
            }
            else{
              geoChartDataArray.push(
                [String(region.ContinentID), region.Percent]
              );
            }
          });

          var randata = [
            ["Region", "Invested"]
          ];

          this.geoChartData =  {
            chartType: 'GeoChart',
            dataTable: geoChartDataArray,
            //dataTable: randata,
            options: {
              'height':320,
              resolution: 'subcontinents',
              colorAxis: {
                values: [0,25,26,50,51,75,76,100],
                colors: ['#f1c83e', '#f1c83e', '#ee6b38', '#ee6b38','#e74e05', '#e74e05', '#e64b39', '#e64b39']
              },
              'backgroundColor':{
                fill:'transparent',
                stroke:'none',
                strokeWidth: 0,
              },
              showTooltip: false,
              showInfoWindow: false,
              enableInteractivity: false,
              'tooltip':{
                trigger: 'none'
              },
              legend: 'none'
            },
          };

          resolve();
        }
      })
    });
  }

  getGraphData(){
    var Dates = [];
    var Yields = [];
    var monthNames = ["Jan", "Feb", "Mar", "Apr", "May","Jun","Jul", "Aug", "Sep", "Oct", "Nov","Dec"];
    var _date;
    this.GraphData.forEach(data =>{
      _date = new Date(data.Date);
      Dates.push(
        //String(_date.getMonth()) + '/' + String(_date.getDate()) + '/' + String(_date.getFullYear())
        String(_date.getDate()) + ' ' + monthNames[_date.getMonth()] + ' ' + String(_date.getFullYear()).split('0')[1]
      );
      Yields.push(
        data.TotalYield
      );
    });
    this.chart = new Chart({

    xAxis: {
      categories: Dates,
      gridLineColor: '#282a38',
      gridLineWidth: 1,
      // labels :{
      //   step : 12
      // },
      tickLength: 0,
      // tickInterval: 30,
      // tickAmount: 30,
      lineColor:'#282a38'
    },
    yAxis: {
      tickInterval: 0.5,
      tickAmount: 7,
      gridLineWidth: 0,
      title: {
        text: ''
      },
      labels: {
        format: '{value}%'
      }
    },

      chart: {
        type: 'spline',
        backgroundColor:'#1e1f2b ',
      },
      title: {
        text: ''
      },
      series: [
        {
          name: 'VIVA Portfolio Yield',
          data: Yields,
          color:'#F5643D'
        },
        {
          name: 'MBS Benchmark Yields',
          data: this.getBenchMarkData(),
          color: '#43BA72'
        }
      ],
      legend: {
        itemStyle: {
          color: '#666666'
        },
        itemHoverStyle: {
          color: '#FFFFFF'
        }
      }
    });
  }

  //#endregion Functions

  //#region Events

  ngOnInit() {
    this.init();
  }

  //#region Fake Benchmark Data

  getBenchMarkData(){
    var data = [
      2.65,
      2.59,
      2.63,
      2.74,
      2.77,
      2.92,
      3.34,
      3.31,
      3.31,
      3.46,
      3.44,
      3.39,
      3.43
    ];

    return data;
  }

  //#endregion Fake Benchmark Data

  //#endregion Events

}


// #4f1a15
// #4f1a15

