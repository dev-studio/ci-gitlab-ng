import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { Router } from '@angular/router';
import { User } from '../../providers/user';
import { Storage } from '../../providers/storage';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

//#region Variables
  isLoading: boolean = false;
  account: { Username: string, Password: string, grant_type: string } = {
    Username: '',
    Password: '',
    grant_type: 'password'
  };
//#endregion Variables

  //#region Constructor
  constructor(
    private userService: User,
    private storage: Storage,
    private router: Router
  ) { }
  //#endregion Constructor

  //#region Functions

  login(){
    this.isLoading = true;
    this.userService.login(this.account).then(res => {
      this.isLoading = false;
      if(res){
        this.storage.get('Token').then(token => {
          if(token)
          this.storage.get('UserInfo').then(userinfo => {
            if(userinfo)
              this.router.navigateByUrl('dashboard/home');
          });
        });
      }
    });
  }

  //#endregion Functions

  //#region Events

  public ngOnInit(){
    var screenHeight = $(window).height();

    $(".auth-main .col-xl-8").css("min-height",screenHeight);
  }

  doLogin(){
    this.login();
  }

  returnKeyPressed(event){
    if(event.key === "Enter"){
      if(this.account.Username && this.account.Password && this.account.Username.includes('@') && this.account.Username.includes('.'))
      this.login();
    }
  }

  //#endregion Events

}
