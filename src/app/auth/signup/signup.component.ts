import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { User } from '../../providers/user';
import { Router } from '@angular/router';
import { Storage } from '../../providers/storage';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  //#region Variables
  
  isLoading:boolean = false;

  account: {
    Email: string,
    FullName: string,
    Password: string,
    ConfirmPassword: string,
    DOB: any
  } = {
    Email: '',
    FullName: '',
    Password: '',
    ConfirmPassword: '',
    DOB: null
  }

  emailAlreadyExists: boolean = false;
  interval: any;

  //#endregion Variables

  //#region Constructor
  constructor(
    private userService: User,
    private router: Router,
    private storage: Storage
  ) { }
  //#endregion Constructor

  //#region Functions

  checkIfEmailExists(){
    this.userService.checkIfEmailExists(this.account.Email).then(res => {
      this.emailAlreadyExists = <boolean>res;
    });
  }

  //#endregion Functions

  //#region Events

  public ngOnInit() {
    var screenHeight = $(window).height();
    $(".auth-main .col-xl-8").css("min-height",screenHeight);
    $('.icon.password').on('mousedown mouseup', function mouseState(e) {
      $(this).prev('input').focus();
        if (e.type == "mousedown") {
            //code triggers on hold
            $(this).prev('input').attr('type','text');
     }
      else{
        $(this).prev('input').attr('type','password');
      }
    });
    $('.password-input').keyup(function(){
      var numberLenght = $(this).val().length;
      $('.password-lenght').css("width",numberLenght*10+"%");
    })
  }

  registerNewUser(){
    this.isLoading = true;
    clearInterval(this.interval);
    this.userService.registerAccount(this.account).then(res => {
      this.isLoading = false;
      if(res){
        this.storage.get('Token').then(token => {
          if(token)
          this.storage.get('UserInfo').then(userinfo => {
            if(userinfo)
              this.router.navigateByUrl('dashboard/home');
          });
        });
      }
    });
  }

  emailExists(){
    clearInterval(this.interval);
    if(this.account.Email.length > 0 && this.account.Email.includes('@') && this.account.Email.includes('.')){
      this.interval = setInterval(() => {
        this.checkIfEmailExists();
      }, 1000);
    } else {
      clearInterval(this.interval);
    }
  }

  strongPassword:boolean = false;
  checkPasswordStrength(){
    this.strongPassword = this.validatePassword();
    console.log(this.strongPassword);
  }

  validatePassword(options = { lenght : 6, lower : true, upper : true, special : true, number : true }) {
    if (this.account.Password.length < (options.lenght ? options.lenght : 6)) return false
    var alphabets = "abcdefghijklmnopqrstuvwxyz";    
    var lower = (options.lower == undefined ? false : !options.lower), upper = (options.upper == undefined ? false : !options.upper), numbers = (options.number == undefined ? false : !options.number), special = (options.special == undefined ? false : !options.special);
    // console.log(options,lower,upper,numbers,special);
    this.account.Password.split('').forEach(c => {
        if (!isNaN(parseInt(c))) numbers = true
        else if (c == c.toUpperCase() && alphabets.indexOf(c.toLowerCase()) != -1) upper = true
        else if (c == c.toLowerCase() && alphabets.indexOf(c.toLowerCase()) != -1) lower = true
        else special = true
    });
    return lower && upper && numbers && special
  }

  returnKeyPressed(event){
    if(event.key === "Enter"){
      if(
        this.account.Email && 
        this.account.Password && 
        this.account.ConfirmPassword &&
        this.account.Email.includes('@') && 
        this.account.Email.includes('.') &&
        this.account.FullName &&
        !this.emailAlreadyExists
      )
      this.registerNewUser();
    }
  }

  //#endregion Events

}
