import { Component, OnInit } from '@angular/core';
import { Chart } from 'angular-highcharts';
import { User } from '../../providers/user';
import { API } from '../../providers/api';
import { Headers, RequestOptions } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';
declare var $;



@Component({
  selector: 'app-my-investment',
  templateUrl: './my-investment.component.html',
  styleUrls: ['./my-investment.component.scss'],

})
export class MyInvestmentComponent implements OnInit {

  public lottieConfig: Object = {
    path: 'assets/svg/confirm-icon.json',
    autoplay: true,
    loop: false
};

  //#region Variables

  USER_INFO: any;
  AUTH_INFO: any;

  isLoading:boolean = false;

  lstInvestments: any[];
  summary:
  {
    Quantity: number,
    MarketValue: number,
    UnrealizedGainsLosses: number,
    AccuredInterest: number,
    EstimatedAnnualIncome: number,
    EstimatedYield: number
  } = {
    Quantity: 0,
    MarketValue: 0,
    UnrealizedGainsLosses: 0,
    AccuredInterest: 0,
    EstimatedAnnualIncome: 0,
    EstimatedYield: 0
  }

  //#endregion Variables

  //#region Constructor
  constructor(
    private userServices: User,
    private api: API,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { }
  //#endregion Constructor

  //#region Functions

  init(){
    this.getPreRequisites().then(res => {
      this.getMyInvestments();
    });
  }

  getPreRequisites(){
    this.activatedRoute.queryParams.subscribe(params => {
      if(params['invested']){
        console.log("Show PopUp");
        this.showPopupInvestmentSuccessPopUp();
      }
      else{
        console.log("Dont Show Popup");
      }
  });
    return new Promise(resolve => {
      this.isLoading = true;
      this.userServices.getTokenFromStorage().then(token => {
        if(token){
          this.AUTH_INFO = token;
          this.userServices.getUserInfoFromStorage().then(userInfo => {
            if(userInfo){
              this.USER_INFO = userInfo;
              resolve(true);
            }
          })
        }
      }).catch( err => {
        this.router.navigateByUrl('login');
      });;
    });
  }

  showPopupInvestmentSuccessPopUp(){
    $('#buyOrderModal').modal('show');
  }

  getMyInvestments(){
    return new Promise(resolve => {
      let _headers = new Headers({ 'Authorization': 'bearer ' + this.AUTH_INFO.access_token });
      let _options = new RequestOptions({ headers: _headers });
      this.api.get('api/Investment/GetListOfInvestment?userid=' + this.USER_INFO.UserInfo.UserID, null, _options).subscribe(result => {
      if(result)
          this.lstInvestments = [];
          this.lstInvestments = result.json().Result;
          if(this.lstInvestments.length > 0){
            this.summary.EstimatedYield = result.json().Yield;
            this.calculateSummary();
          }
          this.isLoading = false;
          resolve();
      });
    });
  }

  calculateSummary(){
    this.lstInvestments.forEach(investment => {
      this.summary.Quantity += investment.Quantity;
      this.summary.MarketValue += (investment.MarketPrice * investment.Quantity);
      this.summary.EstimatedAnnualIncome += investment.AnnualIncome;
      this.summary.UnrealizedGainsLosses += (investment.MarketPrice * investment.Quantity) - (investment.UnitCost * investment.Quantity);
      this.summary.AccuredInterest += investment.AccruedInterest;
    });
  }

  //#endregion Functions

  //#region Events

  ngOnInit() {
    this.init();
    // Jquery for content scroll
    var screenHeightv = $(window).height() - 300;
    $(".scroll-content").css("height",screenHeightv);


    // var animation = bodymovin.loadAnimation({
    //   container: document.getElementById('svg'),
    //   renderer: 'svg',
    //   loop: false,
    //   autoplay: true,
    //   path: './assets/svg/conform-icon.json'
    // });

  }

  chart = new Chart({
    xAxis: {
      gridLineColor: '#282a38',
      gridLineWidth: 1,
      // labels :{
      //   step : 12
      // },
      tickLength: 0,
      // tickInterval: 30,
      // tickAmount: 30,
      lineColor:'#282a38'
    },
    yAxis: {
      tickInterval: 0.5,
      tickAmount: 4,
      gridLineWidth: 0,
      title: {
        text: ''
      },
      labels: {
        format: '{value}%'
      }
    },
    chart: {
      type: 'spline',
      backgroundColor:'#1e1f2b',
    },
    title: {
      text: ''
    },
    credits: {
      enabled: false
    },
    legend: {
      itemStyle: {
        color: '#666666'
      },
      itemHoverStyle: {
        color: '#FFFFFF'
      }
    },
    series: [
      {
        name: 'Line 1',
        data: [1, 2, 3]
      }
    ]
  });

  //#endregion Events

}
