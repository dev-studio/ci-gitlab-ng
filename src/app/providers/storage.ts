import { LocalStorage } from '@ngx-pwa/local-storage';
import { Injectable } from '@angular/core';
 
@Injectable()
export class Storage {
 
  constructor(public storage: LocalStorage){
 
  }
 
  get(key){
    return new Promise(resolve => {
      this.storage.getItem(key).subscribe(
      (data) => {
        resolve(data);
      });
    });
  }
 
  save(key, data){
    return new Promise(resolve => {
      this.storage.setItem(key, data).subscribe(() => {
        resolve(true);
      });
    });
  }

  clearAll(){
    return new Promise(resolve => {
      this.storage.clear().subscribe(() => {
        resolve(true);
      });
    });
  }

  clear(key){
    return new Promise(resolve => {
      this.storage.removeItem(key).subscribe(() => {
        resolve(true);
      });
    });
  }
 
}