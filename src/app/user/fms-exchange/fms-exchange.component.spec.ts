import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FmsExchangeComponent } from './fms-exchange.component';

describe('FmsExchangeComponent', () => {
  let component: FmsExchangeComponent;
  let fixture: ComponentFixture<FmsExchangeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FmsExchangeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FmsExchangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
