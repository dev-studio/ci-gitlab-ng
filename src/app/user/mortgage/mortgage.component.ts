import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { User } from '../../providers/user';
import { API } from '../../providers/api';
import { Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { SnotifyService, SnotifyPosition } from 'ng-snotify';
import { Router } from '@angular/router';

@Component({
  selector: 'app-mortgage',
  templateUrl: './mortgage.component.html',
  styleUrls: ['./mortgage.component.scss']
})
export class MortgageComponent implements OnInit {

  //#region Variables
  toggleSort:boolean = false;
  USER_INFO: any;
  AUTH_INFO: any;

  isLoading:boolean = false;
  isPurchaseLoading:boolean = false;
  connectionErrorText: string;

  lstMortgages: any[];
  lstAllMortgages: any[];
  lstRegionsFilter: any[];
  selectedMortgage: any;
  selectedQuantity: number = 0;

  //#endregion Variables

  //#region Constructor

  constructor(
    private userServices: User,
    private api: API,
    private notification: SnotifyService,
    private router: Router
  ) {
    this.connectionErrorText = 'There was a problem while fetching your account information. Please check your internet connection and try again';
  }

  //#endregion Constructor

  //#region Functions

  init(){
    this.getPreRequisites();
  }

  countdownTimer(date){
    var countDownDateOriginal = new Date(date);
    countDownDateOriginal.setDate(countDownDateOriginal.getDate() + 10);
    var countDownDate = countDownDateOriginal.getTime();

    // Update the count down every 1 second
    var daysHTML;
    var hoursHTML;
    var mintsHTML;
    var secondsHTML;
if($(window).width() >= 1700){
  daysHTML = "<span style='display: block;font-size: 12px;'>Days</span>";
  hoursHTML = "<span style='display: block;font-size: 12px;'>Hours</span>";
  mintsHTML = "<span style='display: block;font-size: 12px;'>Mins</span>";
  secondsHTML = "<span style='display: block;font-size: 12px;'>Secs</span>";
}
else{
  daysHTML = "<span style='display: block;font-size: 12px;'>D</span>";
  hoursHTML = "<span style='display: block;font-size: 12px;'>H</span>";
  mintsHTML = "<span style='display: block;font-size: 12px;'>M</span>";
  secondsHTML = "<span style='display: block;font-size: 12px;'>S</span>";
}


    var x = setInterval(function() {

        // Get todays date and time
        var now = new Date().getTime();

        // Find the distance between now and the count down date
        var distance = countDownDate - now;

        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        // Output the result in an element with id="demo"
        $('.days').html(days+daysHTML);
        $('.hours').html(hours+hoursHTML)
        $('.mins').html(minutes+mintsHTML)
        $('.secs').html(seconds+secondsHTML)
        //document.getElementsByClassName("days").innerHTML = days;

        // If the count down is over, write some text
        if (distance < 0) {
            clearInterval(x);
            document.getElementById("demo").innerHTML = "EXPIRED";
        }
    }, 1000);
  }

  getPreRequisites(){
    return new Promise(resolve => {
      this.isLoading = true;
      this.userServices.getTokenFromStorage().then(token => {
        if(token){
          this.AUTH_INFO = token;
          this.userServices.getUserInfoFromStorage().then(userInfo => {
            if(userInfo){
              this.USER_INFO = userInfo;
              this.getMortgages();
              resolve(true);
            }
          })
        }
      }).catch( err => {
        this.router.navigateByUrl('login');
      });;
    });
  }

  getMortgages(){
    return new Promise(resolve => {
      let _headers = new Headers({ 'Authorization': 'bearer ' + this.AUTH_INFO.access_token });
      let _options = new RequestOptions({ headers: _headers });
      this.api.get('api/Mortgage/GetListOfMortgages?userid=' + this.USER_INFO.UserInfo.UserID, null, _options).subscribe(result => {
      if(result){
          this.lstMortgages = result.json();
          this.lstAllMortgages = this.lstMortgages;
          this.selectedMortgage = this.lstMortgages[0];
          this.fillFilterByRegionData();
          this.countdownTimer(this.USER_INFO.UserInfo.AddedDateTime);
          this.isLoading = false;
          resolve();
        }
      })
    });
  }

  invesetMortgageByUser(){
    return new Promise(resolve => {
      let _headers = new Headers({ 'Authorization': 'bearer ' + this.AUTH_INFO.access_token });
      let _options = new RequestOptions({ headers: _headers });
      let obj = {
        UserID: this.USER_INFO.UserInfo.UserID,
        Units: this.selectedQuantity,
        UnitCost: 100.00,
        MortgageID: this.selectedMortgage.MortgageID
      };
      let params = new URLSearchParams();
      for(let p in obj){
          params.set(p, obj[p]);
      }
      this.api.post('api/Investment/BuySharesFromMortgages', params, _options).subscribe(
        result => {
          if(result){
              resolve(true);
            }
        },
        err => {
          this.notification.error(this.connectionErrorText, {
            timeout: 5000,
            showProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            position: SnotifyPosition.rightTop
          });
          resolve(false);
        }
      )
    });
  }

  fillFilterByRegionData(){
    this.lstRegionsFilter = [];
    this.lstMortgages.forEach(mortgage => {
      console.log(mortgage);
      if(this.lstRegionsFilter.indexOf(mortgage.Region) < 0){
        this.lstRegionsFilter.push(mortgage.Region);
      }
    });
  }

  showMortgageDetails(mortgage){
    this.selectedMortgage = mortgage;
  }

  hideDialog(){
    let modal = <HTMLInputElement>document.getElementById('fmsOrderModal');
    let backdrop = <HTMLInputElement>document.getElementsByClassName('modal-backdrop')[0];
    let body = <HTMLBodyElement>document.getElementsByTagName('body')[0];
    modal.classList.remove('show');
    modal.attributes.setNamedItem(modal.attributes.getNamedItem('style')).value = 'display: none';
    modal.setAttribute('aria-hidden', 'true');
    backdrop.remove();
    body.classList.remove('modal-open');
    body.attributes.removeNamedItem('style');
  }

  //#endregion Functions

  //#region Events

  public ngOnInit() {
    this.init();

    // Jquery for content scroll
    var screenHeightv = $(window).height() - 300;
    $(".scroll-content").css("height",screenHeightv);

  }

  openInvestmentDetailsDialog(mortgage){
    this.showMortgageDetails(mortgage);
  }

  confirmInvestmentPurchase(){
    this.isPurchaseLoading = true;
    this.invesetMortgageByUser().then(res => {
      if(res){
        this.isPurchaseLoading = false;
        this.hideDialog();
        this.router.navigateByUrl('dashboard/investment?invested=true');
      }
    });
  }

  getCountDownValue(date){
    this.countdownTimer(date);
  }

  checkInputForFloatingNumbers(){
    var result = String(this.selectedQuantity).includes('.');
    if(result){
      this.selectedQuantity = Number(String(this.selectedQuantity).split('.')[0]);
    }
  }

  toggleCheck = [
    false,
    false,
    false,
    false,
    false,
    false,
    false
  ];

  sorting(index, type){
    this.toggleCheck[index] = !this.toggleCheck[index];
  //this.sortingData=!this.sortingData;
  if(this.toggleCheck[index])
      this.sortByDataType(type, 'asc');
    else
      this.sortByDataType(type, 'desc');
  }


  sortByData(dataType){
    
  }

  selectedRegion = '';
  applyFilter(region){
    this.lstMortgages = [];
    this.lstAllMortgages.forEach(mortgage => {
      if(mortgage.Region === region){
        this.lstMortgages.push(mortgage);
      }
    });
    this.selectedMortgage = this.lstMortgages[0];
    this.selectedRegion = region;
  }

  clearFilter(){
    this.lstMortgages = this.lstAllMortgages;
    this.selectedMortgage = this.lstMortgages[0];
    this.selectedRegion = '';
  }

  sortByDataType(type, order){
    this.lstMortgages = this.lstAllMortgages;
    switch(type){
      case 'Coupon':
        this.lstMortgages = this.lstMortgages.sort((a,b) => {
          if(order === 'asc') 
            return a.Coupon - b.Coupon;
          if(order === 'desc')
            return b.Coupon - a.Coupon;
        });
      break;
      case 'Rating':
        this.lstMortgages = this.lstMortgages.sort((a,b) => {
          if(order === 'asc') 
            return a.Rating > b.Rating ? -1 : 1;
          if(order === 'desc')
            return b.Rating < a.Rating ? 1 : -1;
        });
      break;
      case 'Term':
        this.lstMortgages = this.lstMortgages.sort((a,b) => {
          if(order === 'asc') 
            return a.Maturity - b.Maturity;
          if(order === 'desc')
            return b.Maturity - a.Maturity;
        });
      break;
      case 'LoanAmount':
        this.lstMortgages = this.lstMortgages.sort((a,b) => {
          if(order === 'asc') 
            return a.TotalAmount - b.TotalAmount;
          if(order === 'desc')
            return b.TotalAmount - a.TotalAmount;
        });
      break;
      case 'AmountAvailable':
        this.lstMortgages = this.lstMortgages.sort((a,b) => {
          if(order === 'asc') 
            return a.AvailableAmount - b.AvailableAmount;
          if(order === 'desc')
            return b.AvailableAmount - a.AvailableAmount;
        });
      break;
      
    }
    
  }
  //#endregion Events

}
