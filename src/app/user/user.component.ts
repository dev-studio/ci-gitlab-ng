import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { User } from '../providers/user';
import { Router } from '@angular/router';
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  USER_INFO: any;
  AUTH_INFO: any;

  constructor(
    private userServices: User,
    private router: Router
  ) {
  }

  init(){
    this.getPreRequisites().then(res => {
    });
  }

  getPreRequisites(){
    return new Promise(resolve => {
      this.userServices.getTokenFromStorage().then(token => {
        if(token){
          this.AUTH_INFO = token;
          this.userServices.getUserInfoFromStorage().then(userInfo => {
            if(userInfo){
              this.USER_INFO = userInfo;
              resolve(true);
            }              
          })
        }
      }).catch( err => {
        this.router.navigateByUrl('login');
      });;
    });
  }

 public ngOnInit() {
   this.init();
   var screenHeight = $(window).height();
   $(".main-content").css("min-height",(screenHeight+10) - ($('footer').height() + $('header').height()));
  }

}
