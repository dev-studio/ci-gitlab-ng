import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ChartModule } from 'angular-highcharts';
import { LocalStorageModule } from '@ngx-pwa/local-storage';
import { FormsModule } from '@angular/forms';
import { HttpModule } from "@angular/http";
import { SnotifyModule, SnotifyService, ToastDefaults } from 'ng-snotify';
import { LottieAnimationViewModule } from 'ng-lottie';

//#region Own Imports

import { Ng2GoogleChartsModule } from 'ng2-google-charts';
import {HostListener} from '@angular/core';
import { AppComponent } from './app.component';
import { LandingComponent } from './landing/landing.component';
import { UserComponent } from './user/user.component';
import { DashboardComponent } from './user/dashboard/dashboard.component';
import { NavComponent } from './user/nav/nav.component';
import { FmsExchangeComponent } from './user/fms-exchange/fms-exchange.component';
import { MyInvestmentComponent } from './user/my-investment/my-investment.component';
import { MortgageComponent } from './user/mortgage/mortgage.component';
import { AuthComponent } from './auth/auth.component';
import { LoginComponent } from './auth/login/login.component';
import { SignupComponent } from './auth/signup/signup.component';


//#endregion Own Imports


//#region Custom Providers

import { API } from '../app/providers/api';
import { Storage } from '../app/providers/storage';
import { User } from '../app/providers/user';

//#endregion Custom Providers

const routes: Routes = [
  { path: 'dashboard', component: UserComponent,
    children: [
      {
        path: 'home',
        component: DashboardComponent
      },
      {
        path: 'fms',
        component: FmsExchangeComponent
      },
      {
        path: 'investment',
        component: MyInvestmentComponent
      },
      {
        path: 'mortgage',
        component: MortgageComponent
      }
    ]
},
{ path: '', component: LoginComponent},
{ path: 'login', component: LoginComponent},
{ path: 'signup', component: SignupComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    LandingComponent,
    UserComponent,
    DashboardComponent,
    NavComponent,
    FmsExchangeComponent,
    MyInvestmentComponent,
    MortgageComponent,
    AuthComponent,
    LoginComponent,
    SignupComponent
  ],

  imports: [
    Ng2GoogleChartsModule,
    RouterModule.forRoot(routes),
    BrowserModule,
    FontAwesomeModule,
    ChartModule,
    LocalStorageModule,
    FormsModule,
    HttpModule,
    SnotifyModule,
    LottieAnimationViewModule.forRoot()
  ],
  exports: [ RouterModule ],
  providers: [
    //#region Custom Providers
    API,
    Storage,
    User,
    { provide: 'SnotifyToastConfig', useValue: ToastDefaults},
    SnotifyService
    //#endregion Custom Providers
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
