import { Injectable } from "@angular/core";
import { Http, RequestOptions, Headers, URLSearchParams } from "@angular/http";
import { API } from "./api";
import { Storage } from "./storage";
import { SnotifyService, SnotifyToast, SnotifyPosition, Snotify } from "ng-snotify";

@Injectable()
export class User {

    user: any;
    connectionErrorText: string;

    //#region Constructor
    constructor(
        private http: Http,
        private api: API,
        private storage: Storage,
        private notification: SnotifyService
    ){
        this.connectionErrorText = 'There was a problem while fetching your account information. Please check your internet connection and try again';
    }
    //#endregion Constructor

    //#region Functions

    login(accountInfo: any){
        return new Promise(resolve => {
            let params = new URLSearchParams();
            for(let p in accountInfo){
                params.set(p, accountInfo[p]);
            }

            let _headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8' });
            let _options = new RequestOptions({ headers: _headers});

            this.api.post('token', params, _options).subscribe(
                response => {
                    this.storeToken(response.json()).then(stored => {
                        if(stored){
                            this.getUserInfo().then(userinfo => {
                                if(userinfo)
                                    resolve(true);
                            });
                        }
                    });
                },
                err => {
                    switch(err.status){
                        case 400:
                            this.notification.error('E-mail or Password is incorrect', {
                                timeout: 3000,
                                showProgressBar: false,
                                closeOnClick: true,
                                pauseOnHover: false,
                                position: SnotifyPosition.rightTop
                            });
                            resolve(false);
                        break;
                        default:
                            this.notification.error(this.connectionErrorText, {
                                timeout: 5000,
                                showProgressBar: false,
                                closeOnClick: true,
                                pauseOnHover: true,
                                position: SnotifyPosition.rightTop
                            });
                            resolve(false);
                        break;
                    }
                });
        });
    }

    getUserInfo() {
        return new Promise(resolve => {
          this.getTokenFromStorage().then(res => {
            var token: any = res;
            let _headers = new Headers({ 'Authorization': 'bearer ' + token.access_token });
            let _options = new RequestOptions({ headers: _headers });
            this.api.get('api/Account/UserInfo', null, _options).subscribe(result => {
              this.storeUserInfo(result.json()).then(resposne => {
                resolve(resposne);
              });
            }),
              err => {
                this.notification.error(this.connectionErrorText, {
                    timeout: 5000,
                    showProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    position: SnotifyPosition.rightTop
                });
                resolve(false);
              }
          });
        });
      }

    
    registerAccount(accountInfo: any){
        return new Promise(resolve => {
            let registerObj = {
                Email: accountInfo.Email,
                Password: accountInfo.Password,
                ConfirmPassword: accountInfo.ConfirmPassword
            };
            let params = new URLSearchParams();
            for(let p in registerObj){
                params.set(p, registerObj[p]);
            }

            let _headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8' });
            let _options = new RequestOptions({ headers: _headers});

            this.api.post('api/Account/Register', params, _options).subscribe(
                response => {
                    var data = response.json();
                    this.createNewUserProfile(data, accountInfo).then(res => {
                        if(res)
                            resolve(true);
                    });
                },
                err => {
                    this.notification.error(this.connectionErrorText, {
                        timeout: 5000,
                        showProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        position: SnotifyPosition.rightTop
                    });
                    resolve(false);
                });
        });
    }

    createNewUserProfile(aspNetUserID: string, accountInfo: any){
        return new Promise(resolve => {
            var accountObj = {
                AspNetUserID: aspNetUserID,
                FirstName: accountInfo.FullName.split(' ')[0],
                LastName: accountInfo.FullName.split(' ')[1],
                RoleID: 2
            };

            let params = new URLSearchParams();
            for(let p in accountObj){
                params.set(p, accountObj[p]);
            }

            let _headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8' });
            let _options = new RequestOptions({ headers: _headers});

            this.api.post('api/User/CreateNewUser', params, _options).subscribe(
                response => {
                    var data = response.json();
                    var loginObj = {
                        Username: accountInfo.Email,
                        Password: accountInfo.Password,
                        grant_type: 'password'
                    }
                    this.login(loginObj).then(res => {
                        resolve(true);
                    });
                },
                err => {
                    this.notification.error(this.connectionErrorText, {
                        timeout: 5000,
                        showProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        position: SnotifyPosition.rightTop
                    });
                    resolve(false);
                });
        });
    }

    checkIfEmailExists(email: string){
        return new Promise(resolve => {
            this.api.get('api/User/CheckIfEmailExists?email=' + email, null).subscribe(result => {
            if(result)
                resolve(result.json());
            })
        });
    }


    //#endregion Functions

    //#region Storage Management

    storeToken(data){
        return new Promise(resolve => {
            this.storage.clear('Token').then(removed => {
                if(removed){
                    this.storage.save('Token', data).then(res => {
                        if(res)
                            resolve(true);
                    });
                }
            });
        });
    }

    storeUserInfo(data){
        return new Promise(resolve => {
            this.storage.clear('UserInfo').then(removed => {
                if(removed){
                    this.storage.save('UserInfo', data).then(res => {
                        if(res)
                            resolve(true);
                    });
                }
            });
        });
    }

    getTokenFromStorage(){
        return new Promise((resolve,reject) => {
            this.storage.get('Token').then(token => {
                if(token)
                    resolve(token);
                else
                    reject("Error");    
            });
        });
    }

    getUserInfoFromStorage(){
        return new Promise(resolve => {
            this.storage.get('UserInfo').then(user => {
                if(user)
                    resolve(user);
            });
        });
    }

    //#endregion Storage Management
}